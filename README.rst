The kwgis Module
================



|Pypi| |PypiDownloads|



.. |Pypi| image:: https://img.shields.io/pypi/v/kwgis.svg
    :target: https://pypi.python.org/pypi/kwgis

.. |PypiDownloads| image:: https://img.shields.io/pypi/dm/kwgis.svg
    :target: https://pypistats.org/packages/kwgis