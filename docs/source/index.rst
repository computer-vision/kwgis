:gitlab_url: https://gitlab.kitware.com/computer-vision/kwgis

.. The large version wont work because github strips rst image rescaling. https://i.imgur.com/AcWVroL.png
    # TODO: Add a logo
    .. image:: https://i.imgur.com/PoYIsWE.png
       :height: 100px
       :align: left

Welcome to kwgis's documentation!
=================================

.. The __init__ files contains the top-level documentation overview
.. automodule:: kwgis.__init__
   :show-inheritance:

.. toctree::
   :maxdepth: 5

   kwgis


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`