"""
This script moves utilities from geowatch into kwgis to setup the initial state
of the repo.
"""


def main():
    import ubelt as ub
    import kwutil
    src_repo_dpath = ub.Path('~/code/geowatch').expand()
    src_mod_name = 'geowatch'
    src_mod_dpath = src_repo_dpath / src_mod_name

    dst_repo_dpath = ub.Path('~/code/kwgis').expand()
    dst_mod_dpath = dst_repo_dpath / 'kwgis'

    to_copy = [
        src_mod_dpath / 'gis',
        src_mod_dpath / 'demo',
        src_mod_dpath / 'stac',
        src_mod_dpath / 'utils' / 'util_bands.py',
        src_mod_dpath / 'utils' / 'util_gdal.py',
        src_mod_dpath / 'utils' / 'util_rgdc.py',
        src_mod_dpath / 'utils' / 'util_gis.py',
        src_mod_dpath / 'utils' / 'util_retry.py',
        src_mod_dpath / 'exceptions.py',
        src_mod_dpath / 'cli/special/finish_install',
    ]

    copyman = kwutil.CopyManager()
    dst_mod_dpath

    # Delete everything not in the blocklist
    blocklist = [
        'rc'
    ]
    for path in dst_mod_dpath.ls():
        if path.name not in blocklist:
            path.delete()

    for src_path in to_copy:
        rel_path = src_path.relative_to(src_mod_dpath)
        dst_path = dst_mod_dpath / rel_path
        assert src_path.exists()
        copyman.submit(src_path, dst_path)

    copyman.run()
    init_fpath = dst_mod_dpath / '__init__.py'
    init_fpath.write_text('')

    import xdev
    walker = xdev.DirectoryWalker(dst_mod_dpath, exclude_dnames=['__pycache__'], include_fnames=['*.py'])
    walker.build()
    walker.write_report()

    fpaths = sorted([node for node, data in walker.graph.nodes(data=True) if data['isfile']])

    import parso

    def walk_nodes(node):
        yield node
        try:
            for child in node.children:
                yield from walk_nodes(child)
        except AttributeError:
            ...

    def iter_accessed_attrs(module):
        used_attrs = []
        for node in walk_nodes(module):
            if node.type == 'atom_expr':
                child1 = node.children[0]
                if child1.type == 'name':
                    if child1.value == src_mod_name:
                        accessed = [child1]
                        for child in node.children[1:]:
                            keep_going = False
                            if child.type == 'trailer':
                                if child.children[0].type == 'operator':
                                    if child.children[0].value == '.':
                                        keep_going = True
                            if not keep_going:
                                break
                            accessed.append(child)
                        used_attr = parso.python.tree.PythonNode(node.type, accessed).get_code()
                        used_attrs.append(used_attr)
        return used_attrs

    all_imports = []
    used_attrs = []
    modules = {}
    import xdoctest

    for fpath in fpaths:
        text = fpath.read_text()
        module = parso.parse(text)
        modules[fpath] = module
        all_imports += list(module.iter_imports())
        used_attrs += list(iter_accessed_attrs(module))

        doctests = list(xdoctest.core.parse_doctestables(str(fpath)))
        for doctest in doctests:
            doctest_src = doctest.format_src(colored=False, linenos=0, prefix=0)
            doctest_module = parso.parse(doctest_src)
            all_imports += list(doctest_module.iter_imports())
            used_attrs += list(iter_accessed_attrs(doctest_module))
            ...

    type_to_node = ub.group_items(all_imports, key=lambda x: x.type)
    used_modules = []
    for imp in type_to_node['import_name']:
        name = None
        for child in imp.children:
            if child.type == 'keyword':
                ...
            elif child.type == 'dotted_as_name':
                assert name is None
                name = child.children[0].value
            elif child.type == 'name':
                assert name is None
                name = child.value
            else:
                raise Exception(child.type)
        used_modules.append(name)

    requires_port = []
    for imp in type_to_node['import_from']:
        name = None
        for child in imp.children:
            if child.type == 'keyword':
                if child.value == 'import':
                    break
            elif child.type == 'dotted_name':
                assert name is None
                name = child.children[0].value
            elif child.type == 'name':
                assert name is None
                name = child.value
            else:
                raise Exception(child.type)
        if name == src_mod_name:
            requires_port.append(imp)
        used_modules.append(name)

    used_modules = sorted(set(used_modules))
    print(f'requires_port = {ub.urepr(requires_port, nl=1)}')

    code_imports = []
    for part in requires_port:
        code_imports.append(part.get_code(0))
    sorted(set(code_imports))
    (dst_mod_dpath / 'demo/smart_kwcoco_demodata.py').delete()

    xdev.sed('geowatch', 'kwgis', dpath=dst_mod_dpath)
    xdev.sed("'coerce_kwcoco', *", '', dpath=dst_mod_dpath)

    (dst_mod_dpath / '__init__.py').write_text(ub.codeblock(
        '''
        __version__ = '0.1.0'
        '''))

    ((dst_mod_dpath / 'utils').ensuredir() / '__init__.py').write_text(ub.codeblock(
        '''
        '''))

if __name__ == '__main__':
    """
    CommandLine:
        python ~/code/kwgis/dev/port_kwgis_from_geowatch.py
    """
    main()
